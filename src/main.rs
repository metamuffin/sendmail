/*
    This file is part of sendmail (https://codeberg.org/metamuffin/sendmail)
    which is licensed under the GNU Affero General Public License (version 3).
    Copyright (C) 2023 metamuffin <metamuffin.org>
*/
pub mod config;

use clap::{Parser, ValueEnum};
use lettre::{
    address::AddressError, message::Mailbox, transport::smtp::authentication::Credentials,
    SmtpTransport, Transport,
};
use log::{debug, info};
use serde::Deserialize;
use std::{
    io::{read_to_string, stdin},
    path::PathBuf,
    str::FromStr,
    time::Duration,
};
use thiserror::Error;

#[rustfmt::skip]
#[derive(Parser)]
#[clap(about)]
/// Sends electronic mail
struct Args {
    /// Path to the configuration file
    #[arg(short = 'C', long)] config: Option<PathBuf>,
    
    /// Sets the `From` header
    #[arg(short, long)] from: String,
    /// Sets the `To` header
    #[arg(short, long)] to: String,
    /// Sets the `Cc` header
    #[arg(short, long)] cc: Option<String>,
    /// Sets the `Bcc` header
    #[arg(short, long)] bcc: Option<String>,
    /// Sets the `Subject` header
    #[arg(short, long)] subject: Option<String>,
    
    
    /// Sets the SMTP server encryption mechanism
    #[arg(long)] smtp_auth: Option<SmtpAuth>,
    /// Sets the SMTP server port
    #[arg(long)] smtp_port: Option<u16>,
    /// Sets the SMTP server address
    #[arg(long)] smtp_server: Option<String>,
    /// Sets the SMTP server timeout
    #[arg(long)] smtp_timeout: Option<f64>,
    /// Sets the SMTP server authentification credentials (username)
    #[arg(long)] smtp_username: Option<String>,
    /// Sets the SMTP server authentification credentials (password)
    #[arg(long)] smtp_password: Option<String>,
}

#[derive(Debug, Deserialize, Default)]
#[serde(rename_all = "kebab-case")]
struct Config {
    smtp_auth: Option<SmtpAuth>,
    smtp_port: Option<u16>,
    smtp_timeout: Option<f64>,
    smtp_server: Option<String>,
    smtp_username: Option<String>,
    smtp_password: Option<String>,
}

#[derive(ValueEnum, Clone, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
enum SmtpAuth {
    Plain,
    Tls,
    Starttls,
}

#[derive(Debug, Error)]
enum Error {
    #[error("mailbox invalid: {0}")]
    MailboxInvalid(#[from] AddressError),
    #[error("io error: {0}")]
    Io(#[from] std::io::Error),
    #[error("email error: {0}")]
    Email(#[from] lettre::error::Error),
    #[error("config format error: {0}")]
    TomlDeser(#[from] toml::de::Error),
    #[error("variable unset: {0}")]
    Var(#[from] std::env::VarError),
    #[error("impossible error")]
    Infailable(#[from] std::convert::Infallible),
    #[error("nameless user")]
    NamelessUser,
    #[error("username contains invalid UTF-8")]
    UsernameInvalid,
    #[error("missing parameter: {0}")]
    MissingParameter(&'static str),
    #[error("smtp transport error: {0}")]
    SmtpTransport(#[from] lettre::transport::smtp::Error),
}

fn main() -> Result<(), Error> {
    env_logger::init_from_env("LOG");
    let args = Args::parse();
    let config = Config::open_or_default(args.config)?;
    debug!("{config:?}");

    let mut message = lettre::Message::builder()
        .from(Mailbox::from_str(&args.from)?)
        .to(Mailbox::from_str(&args.to)?);

    if let Some(subject) = &args.subject {
        message = message.subject(subject)
    }
    if let Some(cc) = &args.cc {
        message = message.cc(Mailbox::from_str(&cc)?)
    }
    if let Some(bcc) = &args.bcc {
        message = message.bcc(Mailbox::from_str(&bcc)?);
    }
    let message = message.body(read_to_string(stdin())?)?;

    let server = args
        .smtp_server
        .or(config.smtp_server)
        .ok_or(Error::MissingParameter("smtp-server"))?;
    let mut transport = match args
        .smtp_auth
        .or(config.smtp_auth)
        .ok_or(Error::MissingParameter("smtp-auth"))?
    {
        SmtpAuth::Plain => SmtpTransport::builder_dangerous(server),
        SmtpAuth::Tls => SmtpTransport::relay(&server)?,
        SmtpAuth::Starttls => SmtpTransport::starttls_relay(&server)?,
    };

    if let Some(username) = args.smtp_username.or(config.smtp_username) {
        let password = args
            .smtp_password
            .or(config.smtp_password)
            .ok_or(Error::MissingParameter("smtp-password"))?;
        transport = transport.credentials(Credentials::new(username, password));
    }
    if let Some(port) = args.smtp_port.or(config.smtp_port) {
        transport = transport.port(port)
    }
    if let Some(timeout) = args.smtp_timeout.or(config.smtp_timeout) {
        transport = transport.timeout(Some(Duration::from_secs_f64(timeout)))
    }
    let transport = transport.build();

    info!("sending...");
    transport.send(&message)?;
    info!("mail sent");
    Ok(())
}
