/*
    This file is part of sendmail (https://codeberg.org/metamuffin/sendmail)
    which is licensed under the GNU Affero General Public License (version 3).
    Copyright (C) 2023 metamuffin <metamuffin.org>
*/
use crate::{Config, Error};
use log::warn;
use std::{
    env, fs,
    path::{Path, PathBuf},
    str::FromStr,
};

impl Config {
    pub fn open_or_default(r#override: Option<PathBuf>) -> Result<Self, Error> {
        if let Some(o) = r#override {
            Self::open_from(&o)
        } else {
            fn rep<T>(context: &'static str, r: Result<T, Error>) -> Result<T, Error> {
                match &r {
                    Err(Error::Io(_)) => (),
                    Err(Error::Var(_)) => (),
                    Err(e) => warn!("cannot open {context}: {e}"),
                    Ok(_) => (),
                }
                r
            }
            Ok(rep("env config", Self::open_env_config())
                .or_else(|_| rep("xdg config", Self::open_xdg_config()))
                .or_else(|_| rep("guessed xdg config", Self::open_config_guess_from_home()))
                .or_else(|_| {
                    rep(
                        "guessed xdg config",
                        Self::open_config_guess_from_username(),
                    )
                })
                .unwrap_or(Self::default()))
        }
    }
    fn open_from(path: &Path) -> Result<Self, Error> {
        Ok(toml::from_str(&fs::read_to_string(path)?)?)
    }
    fn open_env_config() -> Result<Self, Error> {
        Ok(Self::open_from(&Self::var_as_path("SENDMAIL_CONFIG")?)?)
    }
    fn open_xdg_config() -> Result<Self, Error> {
        Ok(Self::open_from(
            &Self::var_as_path("XDG_CONFIG_HOME")?.join("sendmail.toml"),
        )?)
    }
    fn open_config_guess_from_home() -> Result<Self, Error> {
        Ok(Self::open_from(
            &Self::var_as_path("HOME")?.join(".config/sendmail.toml"),
        )?)
    }
    fn open_config_guess_from_username() -> Result<Self, Error> {
        Ok(Self::open_from(
            &PathBuf::from_str(
                users::get_current_username()
                    .ok_or(Error::NamelessUser)?
                    .to_str()
                    .ok_or(Error::UsernameInvalid)?,
            )?
            .join(".config/sendmail.toml"),
        )?)
    }
    fn var_as_path(key: &str) -> Result<PathBuf, Error> {
        Ok(PathBuf::from_str(&env::var(key)?)?)
    }
}
