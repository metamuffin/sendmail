# sendmail

somewhat simple rust program to send electronic mail.

## Usage

```
Sends electronic mail

Usage: sendmail [OPTIONS] --from <FROM> --to <TO> --subject <SUBJECT>

Options:
  -C, --config <CONFIG>                Path to the configuration file
  -f, --from <FROM>                    Sets the `From` header
  -t, --to <TO>                        Sets the `To` header
  -c, --cc <CC>                        Sets the `Cc` header
  -b, --bcc <BCC>                      Sets the `Bcc` header
  -s, --subject <SUBJECT>              Sets the `Subject` header
      --smtp-auth <SMTP_AUTH>          Sets the SMTP server encryption mechanism [possible values: plain, tls, starttls]
      --smtp-port <SMTP_PORT>          Sets the SMTP server port
      --smtp-server <SMTP_SERVER>      Sets the SMTP server address
      --smtp-timeout <SMTP_TIMEOUT>    Sets the SMTP server timeout
      --smtp-username <SMTP_USERNAME>  Sets the SMTP server authentification credentials (username)
      --smtp-password <SMTP_PASSWORD>  Sets the SMTP server authentification credentials (password)
  -h, --help                           Print help
```

## License

`GNU-AGPL-3.0-only`
